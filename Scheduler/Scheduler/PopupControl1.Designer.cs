﻿namespace Scheduler
{
    partial class PopupControl1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CoursePrereqLabel = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.CourseLocationLabel = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.CourseNameLabel = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.CourseEnrollLabel = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.CourseTimeLabel = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.CourseCodeLabel = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.ScheduleViewRemoveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CoursePrereqLabel
            // 
            this.CoursePrereqLabel.AutoSize = true;
            this.CoursePrereqLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CoursePrereqLabel.Location = new System.Drawing.Point(326, 47);
            this.CoursePrereqLabel.Name = "CoursePrereqLabel";
            this.CoursePrereqLabel.Size = new System.Drawing.Size(41, 13);
            this.CoursePrereqLabel.TabIndex = 141;
            this.CoursePrereqLabel.Text = "label33";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(193, 47);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(127, 13);
            this.label34.TabIndex = 140;
            this.label34.Text = "Course Prerequisites:";
            // 
            // CourseLocationLabel
            // 
            this.CourseLocationLabel.AutoSize = true;
            this.CourseLocationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CourseLocationLabel.Location = new System.Drawing.Point(302, 24);
            this.CourseLocationLabel.Name = "CourseLocationLabel";
            this.CourseLocationLabel.Size = new System.Drawing.Size(41, 13);
            this.CourseLocationLabel.TabIndex = 139;
            this.CourseLocationLabel.Text = "label29";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(193, 24);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(103, 13);
            this.label30.TabIndex = 138;
            this.label30.Text = "Course Location:";
            // 
            // CourseNameLabel
            // 
            this.CourseNameLabel.AutoSize = true;
            this.CourseNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CourseNameLabel.Location = new System.Drawing.Point(271, 0);
            this.CourseNameLabel.Name = "CourseNameLabel";
            this.CourseNameLabel.Size = new System.Drawing.Size(41, 13);
            this.CourseNameLabel.TabIndex = 137;
            this.CourseNameLabel.Text = "label27";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(179, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(86, 13);
            this.label28.TabIndex = 136;
            this.label28.Text = "Course Name:";
            // 
            // CourseEnrollLabel
            // 
            this.CourseEnrollLabel.AutoSize = true;
            this.CourseEnrollLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CourseEnrollLabel.Location = new System.Drawing.Point(116, 47);
            this.CourseEnrollLabel.Name = "CourseEnrollLabel";
            this.CourseEnrollLabel.Size = new System.Drawing.Size(41, 13);
            this.CourseEnrollLabel.TabIndex = 135;
            this.CourseEnrollLabel.Text = "label25";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(3, 47);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(113, 13);
            this.label26.TabIndex = 134;
            this.label26.Text = "Course Enrollment:";
            // 
            // CourseTimeLabel
            // 
            this.CourseTimeLabel.AutoSize = true;
            this.CourseTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CourseTimeLabel.Location = new System.Drawing.Point(96, 17);
            this.CourseTimeLabel.Name = "CourseTimeLabel";
            this.CourseTimeLabel.Size = new System.Drawing.Size(41, 13);
            this.CourseTimeLabel.TabIndex = 133;
            this.CourseTimeLabel.Text = "label17";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 17);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 13);
            this.label18.TabIndex = 132;
            this.label18.Text = "Course Times:";
            // 
            // CourseCodeLabel
            // 
            this.CourseCodeLabel.AutoSize = true;
            this.CourseCodeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CourseCodeLabel.Location = new System.Drawing.Point(92, 0);
            this.CourseCodeLabel.Name = "CourseCodeLabel";
            this.CourseCodeLabel.Size = new System.Drawing.Size(41, 13);
            this.CourseCodeLabel.TabIndex = 131;
            this.CourseCodeLabel.Text = "label16";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 13);
            this.label15.TabIndex = 130;
            this.label15.Text = "Course Code:";
            // 
            // ScheduleViewRemoveButton
            // 
            this.ScheduleViewRemoveButton.Location = new System.Drawing.Point(387, 17);
            this.ScheduleViewRemoveButton.Name = "ScheduleViewRemoveButton";
            this.ScheduleViewRemoveButton.Size = new System.Drawing.Size(83, 43);
            this.ScheduleViewRemoveButton.TabIndex = 129;
            this.ScheduleViewRemoveButton.Text = "Add/Remove Course";
            this.ScheduleViewRemoveButton.UseVisualStyleBackColor = true;
            this.ScheduleViewRemoveButton.Click += new System.EventHandler(this.ScheduleViewRemoveButton_Click);
            // 
            // PopupControl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.CoursePrereqLabel);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.CourseLocationLabel);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.CourseNameLabel);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.CourseEnrollLabel);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.CourseTimeLabel);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.CourseCodeLabel);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.ScheduleViewRemoveButton);
            this.Name = "PopupControl1";
            this.Size = new System.Drawing.Size(474, 64);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CoursePrereqLabel;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label CourseLocationLabel;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label CourseNameLabel;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label CourseEnrollLabel;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label CourseTimeLabel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label CourseCodeLabel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button ScheduleViewRemoveButton;
    }
}
