﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Windows.Forms;

namespace Scheduler
{

    //put in own class
    public class MutableTuple<T1, T2>
    {
        public T1 Item1 { get; set; }
        public T2 Item2 { get; set; }

        public MutableTuple(T1 item1, T2 item2)
        {
            Item1 = item1;
            Item2 = item2;
        }
    }

    public class CandidateScheduleClass
    {
        //variables
        public List<MutableTuple<CourseSectionClass, int>> candidateScheduleCourseList = new List<MutableTuple<CourseSectionClass, int>>();
        bool isThereAConflict;
        int colorOfCell;

        public CandidateScheduleClass()
        {
            //there are no conflict once you first initialize this class
            isThereAConflict = false;
            colorOfCell = 0;
        }

        public void addCourse(CourseSectionClass courseToBeAdded)
        {
            var tempTuple = new MutableTuple<CourseSectionClass, int>(courseToBeAdded, 0);

            tempTuple.Item1.rightJustifiedOnCalendar = false;
            if (!(isInSchedule(courseToBeAdded)))
            {
               
                // Add a new course
                candidateScheduleCourseList.Add(tempTuple);
            }
        }


        public void removeCourse(CourseSectionClass course)
        {
            if (isInSchedule(course))
            {
                for (int i = 0; i < candidateScheduleCourseList.Count; i++)
                {
                    //the course section is specific so if the names match up then they are the same. if so remove the course from the schedule
                    if (candidateScheduleCourseList[i].Item1.courseSectionCode == course.courseSectionCode)
                    {
                        //if colorOfCell was more than 0 then this course was in conflict with another
                        if (candidateScheduleCourseList[i].Item2 > 0)
                        {
                            //find the course that this one was in conflict with
                            for (int j = 0; j < candidateScheduleCourseList.Count; j++)
                            {
                                if (candidateScheduleCourseList[j].Item2 == candidateScheduleCourseList[i].Item2 && i != j)
                                {
                                    //resize and change position of every button that the course was in conflict with
                                    for (int k = 0; k < candidateScheduleCourseList[j].Item1.listOfButtons.Count; k++)
                                    {
                                        candidateScheduleCourseList[j].Item1.listOfButtons[k].Size = new Size(candidateScheduleCourseList[j].Item1.listOfButtons[k].Size.Width * 2, candidateScheduleCourseList[j].Item1.listOfButtons[k].Size.Height);
                                        candidateScheduleCourseList[j].Item1.listOfButtons[k].BackColor = Color.LightSkyBlue;
                                        if (candidateScheduleCourseList[j].Item1.rightJustifiedOnCalendar)
                                        {
                                            candidateScheduleCourseList[j].Item1.listOfButtons[k].Location = new Point(candidateScheduleCourseList[j].Item1.listOfButtons[k].Location.X - 48, candidateScheduleCourseList[j].Item1.listOfButtons[k].Location.Y);
                                        }
                                    }
                                    if (candidateScheduleCourseList[j].Item1.rightJustifiedOnCalendar)
                                    {
                                        candidateScheduleCourseList[j].Item1.rightJustifiedOnCalendar = false;
                                    }
                                    candidateScheduleCourseList[j].Item2 = 0;
                                }
                            }
                        }

                        candidateScheduleCourseList.RemoveAt(i);
                    }
                }
                //going to have to also change the colorOfCell for any conflicting classes
            }
        }

        public bool isInSchedule(CourseSectionClass course)
        {
            //check if it is in the list at all
            for (int i = 0; i < candidateScheduleCourseList.Count; i++)
            {
                if (candidateScheduleCourseList[i].Item1.courseSectionCode == course.courseSectionCode)
                {
                    return true;
                }
            }
            return false;
        }

        public CandidateScheduleClass loadSchedule(string filename)
        {
            CandidateScheduleClass schedulePlaceholder = new CandidateScheduleClass();
            CourseList scheduleCourses = new CourseList(filename);
            scheduleCourses.updatePrereq("prereqCourse.csv");

            //add all the course to the candidate schedule
            for (int i = 0; i < scheduleCourses.getNumCourses(); i++)
            {
                schedulePlaceholder.addCourse(scheduleCourses.getCourse(i));
            }

            return schedulePlaceholder;
        }

        public void saveSchedule(string filename)
        {
            StringBuilder csv = new StringBuilder();

            //remember that the first line is skipped over
            var garbageLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", "Course Code", "Short Name", "Long Name", "Start Time", "End Time", "Meeting Time", "Building", "Room", "Enrollment", "Capacity");
            csv.AppendLine(garbageLine);

            for (int i = 0; i < candidateScheduleCourseList.Count(); i++)
            {
                CourseSectionClass currCourse = candidateScheduleCourseList[i].Item1;
                foreach (timePair element in currCourse.times)
                {
                    var newLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}", currCourse.courseSectionCode, currCourse.courseShortName, currCourse.courseLongName, element.getStart(), element.getEnd(), element.meetDays, currCourse.building, currCourse.room, currCourse.enrollment.ToString(), currCourse.capacity.ToString(), currCourse.courseSectionReferenceNumber, currCourse.isPopular.ToString());
                    csv.AppendLine(newLine);
                }
            }

            File.WriteAllText(filename, csv.ToString());
        }

        public int getSize()
        {
            return candidateScheduleCourseList.Count();
        }

        public CourseSectionClass getCourse(int index)
        {
            return candidateScheduleCourseList[index].Item1;
        }

        public int getItem2(int index)
        {
            return candidateScheduleCourseList[index].Item2;
        }

        public int findCourse(string courseCode) //used to find a specific course
        {
            for (int i = 0; i < candidateScheduleCourseList.Count; i++)
            {
                if (candidateScheduleCourseList[i].Item1.courseSectionCode == courseCode)
                {
                    return i;
                }
            }
            return -1;
        }
        public int findCourse(CourseSectionClass course) //used to find a specific course
        {
            for (int i = 0; i < candidateScheduleCourseList.Count; i++)
            {
                if (candidateScheduleCourseList[i].Item1.courseSectionCode == course.courseSectionCode)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
