﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Scheduler
{
    public class timePair
    {
        /////////////////
        /// VARIABLES ///
        /////////////////

        private hourMinTime startTime;
        private hourMinTime endTime;
        public string meetDays { get; private set; }
        public string displayTime { get; private set; }

        /////////////////
        /// FUNCTIONS ///
        /////////////////

        public timePair(string start, string end, string days)
        {
            this.startTime = new hourMinTime(start);
            this.endTime = new hourMinTime(end);
            this.meetDays = days;
            

            //get the value for display time
            if (start == "NULL" || end == "NULL")
            {
                displayTime = "N/A";
            }
            else
            {
                //parse the begin time and the end time
                string[] beginTimeParsed = start.Split(':');
                string[] endTimeParsed = end.Split(':');
                //change the hours from military time
                int placeHolder = Int32.Parse(beginTimeParsed[0]);
                placeHolder = placeHolder % 12;
                if (placeHolder == 0)
                {
                    placeHolder = 12;
                }
                string newBegin = placeHolder.ToString() + ":" + beginTimeParsed[1];

                placeHolder = Int32.Parse(endTimeParsed[0]);
                placeHolder = placeHolder % 12;
                if (placeHolder == 0)
                {
                    placeHolder = 12;
                }
                string newEnd = placeHolder.ToString() + ":" + endTimeParsed[1];
                //check if meet time is " "
                this.displayTime = meetDays + " " + newBegin + " - " + newEnd;
            }
        }

        public bool timeWithin(string startCompare, string endCompare, string days)
        {
            if ((meetDays == days || days == "") && 
            ((startsAtOrAfter(startCompare) && endsAtOrBefore(endCompare)) || (startCompare == "00:00:00" && endCompare == "24:00:00")))
            {
                return true;
            }
            return false;
        }

        public bool startsAtOrAfter(string compareTime) // True if object's start time is greater than or equal to the the argument
        {
            hourMinTime compare = new hourMinTime(compareTime);

            if(startTime.intForm >= compare.intForm)
            {
                return true;
            }
            return false;
        }

        public bool endsAtOrBefore(string compareTime) // True if object's start time is greater than or equal to the the argument
        {
            hourMinTime compare = new hourMinTime(compareTime);

            if (endTime.intForm <= compare.intForm)
            {
                return true;
            }
            return false;
        }

        public string getStart()
        {
            return startTime.stringForm;
        }

        public int getStartMilitaryTime()
        {
            //remove the : from 23:00
            //return Int32.Parse(startTime.stringForm.Replace(":", ""));
            int result = 0;
            Int32.TryParse(startTime.stringForm.Replace(":", ""), out result);
            return result;
        }

        public int getIntFormStart()
        {
            return startTime.intForm;
        }

        public string getEnd()
        {
            return endTime.stringForm;
        }

        public int getEndMilitaryTime()
        {
            //remove the : from 23:00
            int result = 0;
            Int32.TryParse(endTime.stringForm.Replace(":", ""), out result);
            return result;
        }

        public int getIntFormEnd()
        {
            return endTime.intForm;
        }

        private class hourMinTime
        {
            public string stringForm {get; private set;}
            public int intForm {get; private set;}

            public hourMinTime(string newTime) // Makes a string starting in the form "HH:MM" or "H:MM" into a string-int pair
            {
                stringForm = newTime;

                if (newTime.StartsWith("NULL")) // For items such as "study abroad" which have no time slot
                {
                    stringForm = "NULL";
                    intForm = -1;
                    return;
                }

                // Store the number of hours and minutes, ignore the rest of the string
                Regex pattern = new Regex("^(?<hour>.{1,2}):(?<minute>.{1,2}).*$");
                                
                var m = pattern.Match(newTime);

                if (!m.Success)
                {
                    return;
                }

                int tempNum;
                Int32.TryParse(m.Groups["hour"].Value, out tempNum);

                intForm = 60*tempNum; // Add 60 minutes per hour

                Int32.TryParse(m.Groups["minute"].Value, out tempNum);

                intForm += tempNum; // Add the remaining minute

            }
        }
    }
}
