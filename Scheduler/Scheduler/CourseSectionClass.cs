﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Scheduler
{
    public class CourseSectionClass //instead of course class have a courseSection class based on the domain diagram
    {
        /////////////////
        /// VARIABLES ///
        /////////////////

        // All variables are effectively read-only after constructiom
        
        //general course information
        public string courseLongName { get; private set; } //the name of the course. different course sections can have the same course name. this is the longer version

        public string courseShortName { get; private set; } //the name of the course. different course sections can have the same course name. this is the shorter, more condensed version

        public string courseSectionReferenceNumber { get; private set; } //the reference number for the course section. this will be unique and specific for each course section

        public string courseSectionCode { get; private set; }//course section code. this is going to be specific to each course section

        public string courseDescription { get; private set; }//course description. holds a long string describing the course. different course sections can have the same course descriptions

        public string building { get; private set; }//the building the section is to meet in

        public string room { get; private set; }//the room the section is to meet in

        public int capacity { get; private set; }//the capacity of the course section

        public int enrollment { get; private set; }//how many students have enrolled in the course

        public int creditCount { get; private set; }//how many credits the course is

        public bool isPopular { get; private set; } // Whether the course is a popular elective

        public string prerequisite;

        //course section times
        public List<timePair> times = new List<timePair>();

        public List<Button> listOfButtons = new List<Button>();

        public bool rightJustifiedOnCalendar { get; set; }

        /////////////////
        /// FUNCTIONS ///
        /////////////////

        public CourseSectionClass(string longName, string shortName, string refNum, string secCode,
                string description, string building, string room, string capacity, string enrollment,
                string beginTime, string endTime, string meetDays, string isPopular) // Import constructor
        {
            int tempNum = 0; // For transferring parsed strings into ints

            this.courseLongName = longName;
            this.courseShortName = shortName;
            this.courseSectionReferenceNumber = refNum;
            this.courseSectionCode = secCode;
            this.courseDescription = description;
            this.building = building;
            this.room = room;
            Int32.TryParse(capacity, out tempNum); // Capacity: string to int
            this.capacity = tempNum;
            Int32.TryParse(enrollment, out tempNum); // Enrollment: string to int
            this.enrollment = tempNum;
            try
            {
                this.isPopular = Convert.ToBoolean(isPopular);
            }
            catch (FormatException)
            {
                MessageBox.Show("Error in courses.csv: improper boolean\nPlease update your import file with the isPopular column"
                    , "Import error",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.isPopular = Convert.ToBoolean(isPopular);
            timePair newTime = new timePair(beginTime, endTime, meetDays);
            this.times.Add(newTime);
            this.prerequisite = "N/A";
        }

        // Empty constructor
        public CourseSectionClass()
        {
            this.courseLongName = null;
        }

        public List<string> beginTime()
        {
            List<string> allStarts = new List<string>();
            for (int i = 0; i < times.Count; i++)
            {
                allStarts.Add(times[i].getStart());
            }
            return allStarts;
        }

        public List<string> endTime()
        {

            List<string> allEnds = new List<string>();
            for (int i = 0; i < times.Count; i++)
            {
                allEnds.Add(times[i].getEnd());
            }
            return allEnds;
        }

        public void addTime(timePair newTimes)
        {
            this.times.Add(newTimes);
        }

        public string getDisplayTime()
        {
            string allTimes = "";
            for (int i = 0; i < times.Count; i++ )
            {
                allTimes += times[i].displayTime;
                if (i != (times.Count - 1))
                {
                    // Do not add last newline
                    allTimes += Environment.NewLine;
                }
            }
            return allTimes;
        }
    }
}
