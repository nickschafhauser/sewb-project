﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Scheduler
{
    public partial class PopupControl1 : UserControl
    {
        Form1 mainForm;
        CourseList courses;
        CandidateScheduleClass candidateSchedule;
        public PopupControl1(Form1 form, CourseList list, CandidateScheduleClass candidate, CourseSectionClass theClass, string courseCode, string courseLongName, string displayTime, int enrollment, int capacity, string building, string room, string prereq, bool add)
        {
            InitializeComponent();

            CourseCodeLabel.Text = courseCode;
            CourseNameLabel.Text = courseLongName;
            CourseTimeLabel.Text = displayTime;
            CourseEnrollLabel.Text = enrollment + "/" + capacity;
            CourseLocationLabel.Text = building + " " + room;
            CoursePrereqLabel.Text = prereq;

            if (add)
            {
                ScheduleViewRemoveButton.Text = "Add Course";
                if (candidate.isInSchedule(theClass))
                {
                    ScheduleViewRemoveButton.Enabled = false;
                }
            }
            else
            {
                ScheduleViewRemoveButton.Text = "Remove Course";
            }

            mainForm = form;
            courses = list;
            candidateSchedule = candidate;
        }

        //CourseList courses;

        private void ScheduleViewRemoveButton_Click(object sender, EventArgs e)
        {
            if (ScheduleViewRemoveButton.Text == "Add Course")
            {
                //course is to be added
                //add all courses with the same course code
                CourseList sameCourseCode = courses.searchCourseCode(CourseCodeLabel.Text);

                if (!(candidateSchedule.isInSchedule(sameCourseCode.getCourse(0))))
                {
                    //add to the table and the grid for all courses with the same name
                    for (int i = 0; i < sameCourseCode.getNumCourses(); i++)
                    {

                        mainForm.addToCandidateScheduleAndGrid(sameCourseCode.getCourse(i));
                    }
                }

            }
            else
            {
                ////course is to be removed
                CourseSectionClass temp = candidateSchedule.getCourse(candidateSchedule.findCourse(CourseCodeLabel.Text));
                mainForm.removeCourseFromCandidateSchedule(temp);
            }

            ((PopupControl.Popup)Parent).Close();
        }
    }
}
