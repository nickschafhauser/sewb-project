﻿//nick
namespace Scheduler
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.referenceNumbersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AdvFilterButton = new System.Windows.Forms.Button();
            this.AdvFilterGroup = new System.Windows.Forms.GroupBox();
            this.ClearFiltButton = new System.Windows.Forms.Button();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.StartTimeLabel = new System.Windows.Forms.Label();
            this.EndTimeComboBox = new System.Windows.Forms.ComboBox();
            this.StartTimeComboBox = new System.Windows.Forms.ComboBox();
            this.EndTimeLabel = new System.Windows.Forms.Label();
            this.SearchGridView = new System.Windows.Forms.DataGridView();
            this.CourseCodeSearchColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CourseShortNameSearchColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CourseTimeCourseColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddCourseSearchViewColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.SearchBar = new System.Windows.Forms.TextBox();
            this.SearchButton = new System.Windows.Forms.Button();
            this.SearchLabel = new System.Windows.Forms.Label();
            this.CandidateScheduleTableView = new System.Windows.Forms.DataGridView();
            this.CourseCodeCandidateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CourseNameCandidateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CourseTimeCandidateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RemoveCourseCandidateViewButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape7 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.ClearCandidateScheduleButton = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.popularCourseCheckBox = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.AdvFilterGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CandidateScheduleTableView)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1140, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.referenceNumbersToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // referenceNumbersToolStripMenuItem
            // 
            this.referenceNumbersToolStripMenuItem.Name = "referenceNumbersToolStripMenuItem";
            this.referenceNumbersToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.referenceNumbersToolStripMenuItem.Text = "Reference Numbers";
            this.referenceNumbersToolStripMenuItem.Click += new System.EventHandler(this.referenceNumbersToolStripMenuItem_Click);
            // 
            // AdvFilterButton
            // 
            this.AdvFilterButton.Location = new System.Drawing.Point(482, 34);
            this.AdvFilterButton.Name = "AdvFilterButton";
            this.AdvFilterButton.Size = new System.Drawing.Size(104, 23);
            this.AdvFilterButton.TabIndex = 10;
            this.AdvFilterButton.Text = "Advanced Options";
            this.AdvFilterButton.UseVisualStyleBackColor = true;
            this.AdvFilterButton.Click += new System.EventHandler(this.AdvFilterButton_Click);
            // 
            // AdvFilterGroup
            // 
            this.AdvFilterGroup.Controls.Add(this.popularCourseCheckBox);
            this.AdvFilterGroup.Controls.Add(this.label18);
            this.AdvFilterGroup.Controls.Add(this.label15);
            this.AdvFilterGroup.Controls.Add(this.ClearFiltButton);
            this.AdvFilterGroup.Controls.Add(this.checkBox5);
            this.AdvFilterGroup.Controls.Add(this.checkBox4);
            this.AdvFilterGroup.Controls.Add(this.checkBox3);
            this.AdvFilterGroup.Controls.Add(this.checkBox2);
            this.AdvFilterGroup.Controls.Add(this.checkBox1);
            this.AdvFilterGroup.Controls.Add(this.StartTimeLabel);
            this.AdvFilterGroup.Controls.Add(this.EndTimeComboBox);
            this.AdvFilterGroup.Controls.Add(this.StartTimeComboBox);
            this.AdvFilterGroup.Controls.Add(this.EndTimeLabel);
            this.AdvFilterGroup.Enabled = false;
            this.AdvFilterGroup.Location = new System.Drawing.Point(16, 57);
            this.AdvFilterGroup.Name = "AdvFilterGroup";
            this.AdvFilterGroup.Size = new System.Drawing.Size(575, 37);
            this.AdvFilterGroup.TabIndex = 9;
            this.AdvFilterGroup.TabStop = false;
            this.AdvFilterGroup.Visible = false;
            // 
            // ClearFiltButton
            // 
            this.ClearFiltButton.Location = new System.Drawing.Point(497, 9);
            this.ClearFiltButton.Name = "ClearFiltButton";
            this.ClearFiltButton.Size = new System.Drawing.Size(75, 23);
            this.ClearFiltButton.TabIndex = 11;
            this.ClearFiltButton.Text = "Clear Filters";
            this.ClearFiltButton.UseVisualStyleBackColor = true;
            this.ClearFiltButton.Click += new System.EventHandler(this.ClearFiltButton_Click);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox5.Location = new System.Drawing.Point(459, 13);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(32, 17);
            this.checkBox5.TabIndex = 10;
            this.checkBox5.Text = "F";
            this.checkBox5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox4.Location = new System.Drawing.Point(419, 13);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(34, 17);
            this.checkBox4.TabIndex = 10;
            this.checkBox4.Text = "R";
            this.checkBox4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox3.Location = new System.Drawing.Point(376, 13);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(37, 17);
            this.checkBox3.TabIndex = 10;
            this.checkBox3.Text = "W";
            this.checkBox3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox2.Location = new System.Drawing.Point(337, 13);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(33, 17);
            this.checkBox2.TabIndex = 10;
            this.checkBox2.Text = "T";
            this.checkBox2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox1.Location = new System.Drawing.Point(296, 13);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(35, 17);
            this.checkBox1.TabIndex = 9;
            this.checkBox1.Text = "M";
            this.checkBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // StartTimeLabel
            // 
            this.StartTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartTimeLabel.Location = new System.Drawing.Point(-3, 9);
            this.StartTimeLabel.Name = "StartTimeLabel";
            this.StartTimeLabel.Size = new System.Drawing.Size(39, 23);
            this.StartTimeLabel.TabIndex = 6;
            this.StartTimeLabel.Text = "Start";
            this.StartTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EndTimeComboBox
            // 
            this.EndTimeComboBox.FormattingEnabled = true;
            this.EndTimeComboBox.Items.AddRange(new object[] {
            "<None>",
            "08:00:00",
            "09:00:00",
            "10:00:00",
            "11:00:00",
            "12:00:00",
            "13:00:00",
            "14:00:00",
            "15:00:00",
            "16:00:00",
            "17:00:00",
            "18:00:00",
            "19:00:00",
            "20:00:00",
            "21:00:00",
            "22:00:00"});
            this.EndTimeComboBox.Location = new System.Drawing.Point(148, 11);
            this.EndTimeComboBox.Name = "EndTimeComboBox";
            this.EndTimeComboBox.Size = new System.Drawing.Size(76, 21);
            this.EndTimeComboBox.TabIndex = 8;
            this.EndTimeComboBox.SelectedIndexChanged += new System.EventHandler(this.EndTimeComboBox_SelectedIndexChanged);
            this.EndTimeComboBox.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.EndTimeComboBox_Format);
            // 
            // StartTimeComboBox
            // 
            this.StartTimeComboBox.FormattingEnabled = true;
            this.StartTimeComboBox.Items.AddRange(new object[] {
            "<None>",
            "08:00:00",
            "09:00:00",
            "10:00:00",
            "11:00:00",
            "12:00:00",
            "13:00:00",
            "14:00:00",
            "15:00:00",
            "16:00:00",
            "17:00:00",
            "18:00:00",
            "19:00:00",
            "20:00:00",
            "21:00:00",
            "22:00:00"});
            this.StartTimeComboBox.Location = new System.Drawing.Point(38, 11);
            this.StartTimeComboBox.Name = "StartTimeComboBox";
            this.StartTimeComboBox.Size = new System.Drawing.Size(76, 21);
            this.StartTimeComboBox.TabIndex = 5;
            this.StartTimeComboBox.SelectedIndexChanged += new System.EventHandler(this.StartTimeComboBox_SelectedIndexChanged);
            this.StartTimeComboBox.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.StartTimeComboBox_Format);
            // 
            // EndTimeLabel
            // 
            this.EndTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EndTimeLabel.Location = new System.Drawing.Point(115, 9);
            this.EndTimeLabel.Name = "EndTimeLabel";
            this.EndTimeLabel.Size = new System.Drawing.Size(37, 23);
            this.EndTimeLabel.TabIndex = 7;
            this.EndTimeLabel.Text = "End";
            this.EndTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SearchGridView
            // 
            this.SearchGridView.AllowUserToAddRows = false;
            this.SearchGridView.AllowUserToDeleteRows = false;
            this.SearchGridView.AllowUserToResizeRows = false;
            this.SearchGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.SearchGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.SearchGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SearchGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.SearchGridView.ColumnHeadersHeight = 34;
            this.SearchGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.SearchGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CourseCodeSearchColumn,
            this.CourseShortNameSearchColumn,
            this.CourseTimeCourseColumn,
            this.AddCourseSearchViewColumn});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SearchGridView.DefaultCellStyle = dataGridViewCellStyle4;
            this.SearchGridView.GridColor = System.Drawing.SystemColors.ControlLight;
            this.SearchGridView.Location = new System.Drawing.Point(10, 113);
            this.SearchGridView.Name = "SearchGridView";
            this.SearchGridView.ReadOnly = true;
            this.SearchGridView.RowHeadersVisible = false;
            this.SearchGridView.Size = new System.Drawing.Size(576, 394);
            this.SearchGridView.TabIndex = 4;
            this.SearchGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SearchGridView_CellMouseClick);
            this.SearchGridView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SearchGridView_CellMouseDoubleClick);
            // 
            // CourseCodeSearchColumn
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.CourseCodeSearchColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.CourseCodeSearchColumn.HeaderText = "Course Code";
            this.CourseCodeSearchColumn.Name = "CourseCodeSearchColumn";
            this.CourseCodeSearchColumn.ReadOnly = true;
            this.CourseCodeSearchColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CourseCodeSearchColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CourseCodeSearchColumn.Width = 180;
            // 
            // CourseShortNameSearchColumn
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.CourseShortNameSearchColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.CourseShortNameSearchColumn.HeaderText = "Course Name";
            this.CourseShortNameSearchColumn.Name = "CourseShortNameSearchColumn";
            this.CourseShortNameSearchColumn.ReadOnly = true;
            this.CourseShortNameSearchColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CourseShortNameSearchColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CourseShortNameSearchColumn.Width = 200;
            // 
            // CourseTimeCourseColumn
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.CourseTimeCourseColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.CourseTimeCourseColumn.HeaderText = "Course Times";
            this.CourseTimeCourseColumn.Name = "CourseTimeCourseColumn";
            this.CourseTimeCourseColumn.ReadOnly = true;
            this.CourseTimeCourseColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CourseTimeCourseColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CourseTimeCourseColumn.Width = 180;
            // 
            // AddCourseSearchViewColumn
            // 
            this.AddCourseSearchViewColumn.HeaderText = "Add Course";
            this.AddCourseSearchViewColumn.Name = "AddCourseSearchViewColumn";
            this.AddCourseSearchViewColumn.ReadOnly = true;
            this.AddCourseSearchViewColumn.Visible = false;
            this.AddCourseSearchViewColumn.Width = 50;
            // 
            // SearchBar
            // 
            this.SearchBar.Location = new System.Drawing.Point(86, 36);
            this.SearchBar.Name = "SearchBar";
            this.SearchBar.Size = new System.Drawing.Size(308, 20);
            this.SearchBar.TabIndex = 2;
            this.SearchBar.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SearchBar_KeyUp);
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(401, 34);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(75, 23);
            this.SearchButton.TabIndex = 3;
            this.SearchButton.Text = "GO!";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // SearchLabel
            // 
            this.SearchLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchLabel.Location = new System.Drawing.Point(12, 24);
            this.SearchLabel.Name = "SearchLabel";
            this.SearchLabel.Size = new System.Drawing.Size(71, 39);
            this.SearchLabel.TabIndex = 1;
            this.SearchLabel.Text = "Search";
            this.SearchLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CandidateScheduleTableView
            // 
            this.CandidateScheduleTableView.AllowUserToAddRows = false;
            this.CandidateScheduleTableView.AllowUserToDeleteRows = false;
            this.CandidateScheduleTableView.AllowUserToResizeColumns = false;
            this.CandidateScheduleTableView.AllowUserToResizeRows = false;
            this.CandidateScheduleTableView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.CandidateScheduleTableView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.CandidateScheduleTableView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CandidateScheduleTableView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.CandidateScheduleTableView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CandidateScheduleTableView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CourseCodeCandidateColumn,
            this.CourseNameCandidateColumn,
            this.CourseTimeCandidateColumn,
            this.RemoveCourseCandidateViewButton});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CandidateScheduleTableView.DefaultCellStyle = dataGridViewCellStyle8;
            this.CandidateScheduleTableView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.CandidateScheduleTableView.GridColor = System.Drawing.SystemColors.ControlLight;
            this.CandidateScheduleTableView.Location = new System.Drawing.Point(10, 549);
            this.CandidateScheduleTableView.Name = "CandidateScheduleTableView";
            this.CandidateScheduleTableView.ReadOnly = true;
            this.CandidateScheduleTableView.RowHeadersVisible = false;
            this.CandidateScheduleTableView.Size = new System.Drawing.Size(576, 144);
            this.CandidateScheduleTableView.TabIndex = 0;
            this.CandidateScheduleTableView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CandidateScheduleTableView_CellContentClick);
            this.CandidateScheduleTableView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.CandidateScheduleTableView_CellMouseClick);
            this.CandidateScheduleTableView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.CandidateScheduleTableView_CellMouseDoubleClick);
            // 
            // CourseCodeCandidateColumn
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.CourseCodeCandidateColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.CourseCodeCandidateColumn.HeaderText = "Course Code";
            this.CourseCodeCandidateColumn.Name = "CourseCodeCandidateColumn";
            this.CourseCodeCandidateColumn.ReadOnly = true;
            this.CourseCodeCandidateColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CourseCodeCandidateColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CourseCodeCandidateColumn.Width = 180;
            // 
            // CourseNameCandidateColumn
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.CourseNameCandidateColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.CourseNameCandidateColumn.HeaderText = "Course Name";
            this.CourseNameCandidateColumn.Name = "CourseNameCandidateColumn";
            this.CourseNameCandidateColumn.ReadOnly = true;
            this.CourseNameCandidateColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CourseNameCandidateColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CourseNameCandidateColumn.Width = 200;
            // 
            // CourseTimeCandidateColumn
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.CourseTimeCandidateColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.CourseTimeCandidateColumn.HeaderText = "Course Time";
            this.CourseTimeCandidateColumn.Name = "CourseTimeCandidateColumn";
            this.CourseTimeCandidateColumn.ReadOnly = true;
            this.CourseTimeCandidateColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CourseTimeCandidateColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CourseTimeCandidateColumn.Width = 180;
            // 
            // RemoveCourseCandidateViewButton
            // 
            this.RemoveCourseCandidateViewButton.HeaderText = "Remove Course";
            this.RemoveCourseCandidateViewButton.Name = "RemoveCourseCandidateViewButton";
            this.RemoveCourseCandidateViewButton.ReadOnly = true;
            this.RemoveCourseCandidateViewButton.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.RemoveCourseCandidateViewButton.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.RemoveCourseCandidateViewButton.Visible = false;
            this.RemoveCourseCandidateViewButton.Width = 50;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(1022, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 20);
            this.label5.TabIndex = 7;
            this.label5.Text = "Friday";
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(926, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Thursday";
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(830, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Wednesday";
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(734, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Tuesday";
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(638, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Monday";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.shapeContainer2);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(598, 34);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(539, 656);
            this.panel1.TabIndex = 111;
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape5,
            this.lineShape4,
            this.lineShape3,
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer2.Size = new System.Drawing.Size(522, 844);
            this.shapeContainer2.TabIndex = 109;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape5
            // 
            this.lineShape5.Name = "lineShape5";
            this.lineShape5.X1 = 424;
            this.lineShape5.X2 = 424;
            this.lineShape5.Y1 = -159;
            this.lineShape5.Y2 = 841;
            // 
            // lineShape4
            // 
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 328;
            this.lineShape4.X2 = 328;
            this.lineShape4.Y1 = -142;
            this.lineShape4.Y2 = 858;
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 40;
            this.lineShape3.X2 = 40;
            this.lineShape3.Y1 = -103;
            this.lineShape3.Y2 = 897;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 232;
            this.lineShape2.X2 = 232;
            this.lineShape2.Y1 = 0;
            this.lineShape2.Y2 = 1000;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 136;
            this.lineShape1.X2 = 136;
            this.lineShape1.Y1 = 0;
            this.lineShape1.Y2 = 1000;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Gainsboro;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label12.Location = new System.Drawing.Point(0, 372);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(520, 59);
            this.label12.TabIndex = 101;
            this.label12.Text = "    2";
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Gainsboro;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label13.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label13.Location = new System.Drawing.Point(0, 431);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(520, 59);
            this.label13.TabIndex = 102;
            this.label13.Text = "    3";
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Gainsboro;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label11.Location = new System.Drawing.Point(0, 314);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(520, 59);
            this.label11.TabIndex = 100;
            this.label11.Text = "    1";
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.Gainsboro;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label22.Location = new System.Drawing.Point(0, 726);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(520, 59);
            this.label22.TabIndex = 107;
            this.label22.Text = "    8";
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.Gainsboro;
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label21.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label21.Location = new System.Drawing.Point(0, 667);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(520, 59);
            this.label21.TabIndex = 106;
            this.label21.Text = "    7";
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Gainsboro;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label20.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label20.Location = new System.Drawing.Point(0, 608);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(520, 59);
            this.label20.TabIndex = 105;
            this.label20.Text = "    6";
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.Gainsboro;
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label19.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label19.Location = new System.Drawing.Point(0, 549);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(520, 59);
            this.label19.TabIndex = 104;
            this.label19.Text = "    5";
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Gainsboro;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label14.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label14.Location = new System.Drawing.Point(0, 490);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(520, 59);
            this.label14.TabIndex = 103;
            this.label14.Text = "    4";
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.Gainsboro;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label23.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label23.Location = new System.Drawing.Point(0, 785);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(520, 59);
            this.label23.TabIndex = 108;
            this.label23.Text = "    9";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Gainsboro;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label6.Location = new System.Drawing.Point(0, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(520, 59);
            this.label6.TabIndex = 95;
            this.label6.Text = "    8";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Gainsboro;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label7.Location = new System.Drawing.Point(0, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(520, 59);
            this.label7.TabIndex = 96;
            this.label7.Text = "    9";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Gainsboro;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label8.Location = new System.Drawing.Point(0, 137);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(520, 59);
            this.label8.TabIndex = 97;
            this.label8.Text = "   10";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Gainsboro;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label9.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label9.Location = new System.Drawing.Point(0, 196);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(520, 59);
            this.label9.TabIndex = 98;
            this.label9.Text = "   11";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Gainsboro;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label10.Location = new System.Drawing.Point(0, 255);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(520, 59);
            this.label10.TabIndex = 99;
            this.label10.Text = "   12";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape7,
            this.lineShape6});
            this.shapeContainer1.Size = new System.Drawing.Size(1140, 702);
            this.shapeContainer1.TabIndex = 114;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape7
            // 
            this.lineShape7.Name = "lineShape7";
            this.lineShape7.X1 = 6;
            this.lineShape7.X2 = 598;
            this.lineShape7.Y1 = 508;
            this.lineShape7.Y2 = 508;
            // 
            // lineShape6
            // 
            this.lineShape6.Name = "lineShape6";
            this.lineShape6.X1 = 5;
            this.lineShape6.X2 = 599;
            this.lineShape6.Y1 = 94;
            this.lineShape6.Y2 = 94;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(12, 96);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(123, 17);
            this.label16.TabIndex = 129;
            this.label16.Text = "Search Results:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(12, 520);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(111, 17);
            this.label17.TabIndex = 130;
            this.label17.Text = "Your Courses:";
            // 
            // ClearCandidateScheduleButton
            // 
            this.ClearCandidateScheduleButton.Location = new System.Drawing.Point(482, 513);
            this.ClearCandidateScheduleButton.Name = "ClearCandidateScheduleButton";
            this.ClearCandidateScheduleButton.Size = new System.Drawing.Size(104, 30);
            this.ClearCandidateScheduleButton.TabIndex = 131;
            this.ClearCandidateScheduleButton.Text = "Clear Schedule";
            this.ClearCandidateScheduleButton.UseVisualStyleBackColor = true;
            this.ClearCandidateScheduleButton.Click += new System.EventHandler(this.ClearCandidateScheduleButton_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(230, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 12;
            this.label15.Text = "Popular";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(228, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "Courses";
            // 
            // popularCourseCheckBox
            // 
            this.popularCourseCheckBox.AutoSize = true;
            this.popularCourseCheckBox.Location = new System.Drawing.Point(275, 14);
            this.popularCourseCheckBox.Name = "popularCourseCheckBox";
            this.popularCourseCheckBox.Size = new System.Drawing.Size(15, 14);
            this.popularCourseCheckBox.TabIndex = 14;
            this.popularCourseCheckBox.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AcceptButton = this.SearchButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1140, 702);
            this.Controls.Add(this.ClearCandidateScheduleButton);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.CandidateScheduleTableView);
            this.Controls.Add(this.SearchGridView);
            this.Controls.Add(this.AdvFilterButton);
            this.Controls.Add(this.AdvFilterGroup);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.SearchLabel);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.SearchBar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.shapeContainer1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Scheduler";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.AdvFilterGroup.ResumeLayout(false);
            this.AdvFilterGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CandidateScheduleTableView)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ComboBox EndTimeComboBox;
        private System.Windows.Forms.Label EndTimeLabel;
        private System.Windows.Forms.Label StartTimeLabel;
        private System.Windows.Forms.ComboBox StartTimeComboBox;
        private System.Windows.Forms.DataGridView SearchGridView;
        private System.Windows.Forms.TextBox SearchBar;
        private System.Windows.Forms.Button SearchButton;
        public System.Windows.Forms.Label SearchLabel;
        public System.Windows.Forms.DataGridView CandidateScheduleTableView;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
		
		private System.Windows.Forms.Button AdvFilterButton;
        private System.Windows.Forms.GroupBox AdvFilterGroup;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
		
		private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button ClearFiltButton;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape7;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape6;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ToolStripMenuItem referenceNumbersToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseCodeSearchColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseShortNameSearchColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseTimeCourseColumn;
        private System.Windows.Forms.DataGridViewButtonColumn AddCourseSearchViewColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseCodeCandidateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseNameCandidateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseTimeCandidateColumn;
        private System.Windows.Forms.DataGridViewButtonColumn RemoveCourseCandidateViewButton;
        private System.Windows.Forms.Button ClearCandidateScheduleButton;
        private System.Windows.Forms.CheckBox popularCourseCheckBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
    }
}

