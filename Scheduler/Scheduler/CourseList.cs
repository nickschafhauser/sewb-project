﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;

namespace Scheduler
{
    public class CourseList
    {
        public List<CourseSectionClass> course_list = new List<CourseSectionClass>();

        bool first = true; //needed to skip the first line

        public CourseList(string filename) // Constructor, imports the course list from the file arg
        {
            FileStream courseFile = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.None);
            StreamReader reader = new StreamReader(courseFile);

            string nextLine = reader.ReadLine(); // Get the first line

            while (nextLine != null) // Until the EOF is reached
            {
                CourseSectionClass tempCourse = null;

                //the first line should be thrown out
                if (first)
                {
                    first = false;
                }
                else
                {
                    if (parseLine(nextLine, out tempCourse)) // Parse if able, else do not add the course
                    {
                        addCourse(tempCourse);
                    }
                }

                nextLine = reader.ReadLine(); // Get the first line
            }

            if (course_list.Count == 0)
            {
                //TODO: Throw an exception for empty or corrupted import file
            }

            courseFile.Close();
        }

        // Empty constructor, does not import
        public CourseList()
        {}

        public CourseList searchCourseFiltered(string searchTerm, string startTime, string endTime, string days, bool isSearchPopularCourseSelected)
        {
            // Create a list given the current time range and days
            CourseList filteredList = new CourseList();
            if (startTime == "" || startTime == "<None>") // If no filter set
            {
                startTime = "00:00:00";
            }
            if (endTime == "" || endTime == "<None>") // If no filter set
            {
                endTime = "24:00:00";
            }

            for (int i = 0; i < this.getNumCourses(); i++)
            {
                // Check for each course if any time is within the argument times and days
                for (int j = 0; j < course_list[i].times.Count; j++)
                {
                    if (isSearchPopularCourseSelected)
                    {
                        if (this.course_list[i].times[j].timeWithin(startTime, endTime, days) && this.course_list[i].isPopular)
                        {
                            filteredList.addCourse(this.course_list[i]);
                            break;
                        }
                    }
                    else
                    {
                        if (this.course_list[i].times[j].timeWithin(startTime, endTime, days))
                        {
                            filteredList.addCourse(this.course_list[i]);
                            break;
                        }
                    }
                }
            }

            // call SearchCourse with the new list and return the SearchCourse output
            return filteredList.searchCourse(searchTerm);
        }

        // Returns a list of all all courses whose name or section code begins with the arg string (case-insensitive) or with a matching reference number
        public CourseList searchCourse(string searchTerm)
        {
            string[] searchWords = searchTerm.Split(' '); // Split the search term by individual words
            CourseList matchingCourses = new CourseList();
            for (int i = 0; i < this.getNumCourses(); i++)
            {
                bool matches = true;

                for (int j = 0; j < searchWords.Length; j++)
                    if (!(testMatch(searchWords[j], course_list[i].courseSectionCode) || testMatch(searchWords[j], course_list[i].courseShortName)
                    || testMatch(searchWords[j], course_list[i].courseLongName) || testMatch(searchWords[j], course_list[i].courseSectionReferenceNumber)))
                    {
                        matches = false;
                    }

                if (matches)
                {
                    matchingCourses.addCourse(course_list[i]); // Add a course from this list to the search result list
                }
            }
            return matchingCourses;
        }

        // Search course codes for exact match
        public CourseList searchCourseCode(string searchCode)
        {
            CourseList matchingCourses = new CourseList();
            for (int i = 0; i < this.getNumCourses(); i++)
            {
                if (course_list[i].courseSectionCode == searchCode)
                {
                    matchingCourses.addCourse(course_list[i]); // Add a course from this list to the search result list
                }
            }
            return matchingCourses;
        }

        // Test whether any word in the target begins with the given term
        private bool testMatch(string term, string target)
        {
            string[] targetWords = target.Split(' ');

            // If any words in the target begin with the search term, return true
            for (int i = 0; i < targetWords.Length; i++)
            {
                if (targetWords[i].StartsWith(term, true, null)) // Case-insensitive
                {
                    return true;
                }
            }

            return false;
        }

        public int getNumCourses()
        {
            return this.course_list.Count;
        }

        bool parseLine(string dataLine, out CourseSectionClass courseHolder)
        {
            Regex pattern = new Regex("^(?<secCode>.*? {2}.)( *.{0,1}),(?<shortName>\".*?\"|.*?),(?<longName>\".*?\"|.*?),(?<beginTime>.*?),(?<endTime>.*?),(?<meetDays>.*?),(?<building>.*?),(?<roomNum>.*?),(?<enrollment>.*?),(?<capacity>.*?),(?<refNum>.*?),(?<isPopular>.*?)$");

            var m = pattern.Match(dataLine); // Test to see if the input line matches the format
            if (!m.Success) // If non-matching input line, do not import
            {
                courseHolder = new CourseSectionClass();
                return false;
            }

            courseHolder = new CourseSectionClass(m.Groups["longName"].Value, m.Groups["shortName"].Value, m.Groups["refNum"].Value, m.Groups["secCode"].Value,
                    "TestDescription", m.Groups["building"].Value, m.Groups["roomNum"].Value, m.Groups["capacity"].Value, m.Groups["enrollment"].Value,
                    m.Groups["beginTime"].Value, m.Groups["endTime"].Value, m.Groups["meetDays"].Value, m.Groups["isPopular"].Value);

            return true;
        }

        public void updatePrereq(string filename)
        {
            //read in the file
            FileStream courseFile = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.None);
            StreamReader reader = new StreamReader(courseFile);

            string nextLine = reader.ReadLine(); // Get the first line
            nextLine = reader.ReadLine();
            while (nextLine != null) // Until the EOF is reached
            {
                string[] parsed = nextLine.Split(',');
                string courseCode = parsed [0];
                string prereq = parsed[1];

                //interate through courselist and add to courses with prereqs
                for (int i = 0; i < course_list.Count; i++)
                {
                    //StringComparison comp = StringComparison.OrdinalIgnoreCase;
                    if (course_list[i].courseSectionCode.Contains(courseCode))
                    {
                        course_list[i].prerequisite = prereq;
                    }
                }

                nextLine = reader.ReadLine(); // Get the first line
            }

            courseFile.Close();
        }

        // Getters for each index
        public string getCourseLongName(int index)
        {
            return course_list[index].courseLongName;
        }

        public string getCourseShortName(int index)
        {
            return course_list[index].courseShortName;
        }

        public string getCourseReferenceNumber(int index)
        {
            return course_list[index].courseSectionReferenceNumber;
        }

        public string getCourseCode(int index)
        {
            return course_list[index].courseSectionCode;
        }

        public string getCourseDescription(int index)
        {
            return course_list[index].courseDescription;
        }

        public string getCourseBuilding(int index)
        {
            return course_list[index].building;
        }

        public string getCourseRoom(int index)
        {
            return course_list[index].room;
        }

        public int getCourseCapacity(int index)
        {
            return course_list[index].capacity;
        }

        public int getCourseEnrollment(int index)
        {
            return course_list[index].enrollment;
        }

        public int getCourseCredits(int index)
        {
            return course_list[index].creditCount;
        }

        public bool getCoursPopular(int index)
        {
            return course_list[index].isPopular;
        }

        public List<string> getCourseBeginTime(int index)
        {
            List<string> allTimes = new List<string>();
            for (int i = 0; i < course_list[i].times.Count; i++)
            {
                allTimes.Add(course_list[index].times[i].getStart());
            }
            return allTimes;
        }

        public List<string> getCourseEndTime(int index)
        {
            List<string> allTimes = new List<string>();
            for (int i = 0; i < course_list[i].times.Count; i++)
            {
                allTimes.Add(course_list[index].times[i].getEnd());
            }
            return allTimes;
        }

        public List<string> getCourseMeetDays(int index)
        {
            List<string> allDays = new List<string>();
            for (int i = 0; i < course_list[i].times.Count; i++)
            {
                allDays.Add(course_list[index].times[i].meetDays);
            }
            return allDays;
        }

        public string getDisplayTime(int index)
        {
            return course_list[index].getDisplayTime();
        }

        //getter to get entire course
        public CourseSectionClass getCourse(int index)
        {
            return course_list[index];
        }

        public CourseSectionClass getCourseByCode(string code)
        {
            foreach (CourseSectionClass course in course_list)
            {
                if (course.courseSectionCode == code)
                {
                    return course;
                }
            }
            return null;
        }

        // Add a course to the list from another list (hence protected)
        protected void addCourse(CourseSectionClass newCourse)
        {
            bool matching_code = false;
            for (int i = 0; i < this.getNumCourses(); i++)
            {
                if (newCourse.courseSectionCode == this.getCourseCode(i))
                {
                    this.course_list[i].addTime(newCourse.times[0]);
                    matching_code = true;
                    break;
                }
            }
            if (!matching_code) // Only add if course code not found in the current list
            {
                this.course_list.Add(newCourse);
            }
        }
    }
}
