﻿namespace Scheduler
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PrereqTableView = new System.Windows.Forms.DataGridView();
            this.RefTableCourseCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RefTableRefNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.PrereqTableView)).BeginInit();
            this.SuspendLayout();
            // 
            // PrereqTableView
            // 
            this.PrereqTableView.AllowUserToAddRows = false;
            this.PrereqTableView.AllowUserToDeleteRows = false;
            this.PrereqTableView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.PrereqTableView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PrereqTableView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RefTableCourseCode,
            this.RefTableRefNumber});
            this.PrereqTableView.Location = new System.Drawing.Point(11, 12);
            this.PrereqTableView.Name = "PrereqTableView";
            this.PrereqTableView.RowHeadersVisible = false;
            this.PrereqTableView.ShowRowErrors = false;
            this.PrereqTableView.Size = new System.Drawing.Size(258, 237);
            this.PrereqTableView.TabIndex = 0;
            // 
            // RefTableCourseCode
            // 
            this.RefTableCourseCode.HeaderText = "Course Code";
            this.RefTableCourseCode.Name = "RefTableCourseCode";
            this.RefTableCourseCode.ReadOnly = true;
            this.RefTableCourseCode.Width = 120;
            // 
            // RefTableRefNumber
            // 
            this.RefTableRefNumber.HeaderText = "Reference Number";
            this.RefTableRefNumber.Name = "RefTableRefNumber";
            this.RefTableRefNumber.ReadOnly = true;
            this.RefTableRefNumber.Width = 130;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 261);
            this.Controls.Add(this.PrereqTableView);
            this.Name = "Form2";
            this.Text = "Reference Numbers";
            ((System.ComponentModel.ISupportInitialize)(this.PrereqTableView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView PrereqTableView;
        private System.Windows.Forms.DataGridViewTextBoxColumn RefTableCourseCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn RefTableRefNumber;
    }
}